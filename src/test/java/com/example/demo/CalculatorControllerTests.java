package com.example.demo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CalculatorController.class)

public class CalculatorControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void calculatorPath() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2."));
    }

    @Test
    void calculatorOperationsOnePlusOne() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=1+1"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>2"));

    }
    @Test
    void calculatorOperationsOnePlusTwo() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=1+2"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>3"));

    }
    @Test
    void calculatorOperationsOneMinusOne() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=1-1"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>0"));

    }
    @Test
    void calculatorOperationsTwoMinusOne() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=2-1"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>1"));

    }
    @Test
    void calculatorOperationsOneTimesOne() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=1*1"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>1"));

    }
    @Test
    void calculatorOperationsTwoTimesThree() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=2*3"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>6"));

    }
    @Test
    void calculatorOperationsTwoDividedByOne() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=2/1"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>2"));

    }
    @Test
    void calculatorOperationsNineDividedByThree() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculator?calculate=9/3"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.<br><br>3"));

    }
}
