package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(HelloController.class)
public class HelloControllerTests {

    @Autowired
    MockMvc mockMvc;

    @Test
    void helloWorld() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/hello"))
        // Assert
            .andExpect(status().isOk())
                .andExpect(content().string("Hello World"));
    }

    @Test
    void helloWorldWithNameArg() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/hello?name=Mavi"))
        // Assert
            .andExpect(status().isOk())
                .andExpect(content().string("Hello Mavi"));
    }

    @Test
    void calculatorPath() throws Exception {
        // Setup
        // Execution
        mockMvc.perform(get("/calculate"))
                // Assert
                .andExpect(status().isOk())
                .andExpect(content().string("This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2."));
    }


}
