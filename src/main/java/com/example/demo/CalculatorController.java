package com.example.demo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class CalculatorController {
    @GetMapping("/calculator")

    public String calculatorLanding(@RequestParam(required = false) String calculate){
        String result =  "This is the calculator. Enter your query in the URL. For example, <b>/calculator?calculate=8/4</b> will return 2.";
//        System.out.println("calculate: " + calculate);
        if(calculate != null && !calculate.trim().isEmpty()){
            String[] splitExpression = calculate.split("((?<=[+*/-])|(?=[+*/-]))");
            result += "<br><br>";
//            System.out.println("result: " + result);
            int num1 = Integer.parseInt(splitExpression[0]);
            int num2 = Integer.parseInt(splitExpression[2]);
//            System.out.println("split operator: " + splitExpression[1]);
            switch(splitExpression[1]){
                case "+":
//                    System.out.println("in the addition case");
                    result += String.valueOf(num1 + num2);
                    break;
                case "-":
//                    System.out.println("in the subtraction case");
                    result += String.valueOf(num1 - num2);
                    break;
                case "/":
//                    System.out.println("in the division case");
                    result += String.valueOf(num1 / num2);
                    break;
                case "*":
//                    System.out.println("in the multiplication case");
                    result += String.valueOf(num1 * num2);
                    break;
                default:
                    result += "Sorry, I didn't get that. We support the following operators: +, -, * and /.";
            }
        }
        return result;
    }
}
